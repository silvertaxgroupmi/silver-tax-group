The attorneys of our firm — led by Chad C. Silver — bring more than four decades of federal tax defense experience to the legal matters facing our clients. For a competitive price, they can help you find an efficient resolution to your tax issues and keep the IRS from pushing you around.

Address: 1685 Baldwin Rd, Suite 300, Pontiac, MI 48340, USA

Phone: 855-900-1040

Website: https://silvertaxgroup.com
